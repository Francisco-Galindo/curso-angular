import { Component, OnInit, ViewChild } from '@angular/core';
import {ChartData, ChartType} from 'chart.js';
import {BaseChartDirective} from 'ng2-charts';
import {GraficasService} from '../../services/graficas.service';

@Component({
	selector: 'app-dona-http',
	templateUrl: './dona-http.component.html',
	styles: [
	]
})
export class DonaHttpComponent implements OnInit {

	@ViewChild(BaseChartDirective) chart?: BaseChartDirective;

	constructor(private graficasService: GraficasService) { }
	data: Object = {};
	papure: number[] = [];

	ngOnInit(): void {
		this.graficasService.getUsuariosRedesSociales()
			.subscribe(data => {
				this.data = data;
				this.doughnutChartData.labels = Object.keys(this.data);
				this.doughnutChartData.datasets[0].data = Object.values(this.data);
				this.chart?.update();
			})
	}

	public doughnutChartLabels: string[] = [];
	public doughnutChartData: ChartData<'doughnut'> = {
		labels: this.doughnutChartLabels,
		datasets: [
			{ data: this.papure },
		]
	};
	public doughnutChartType: ChartType = 'doughnut';

}
