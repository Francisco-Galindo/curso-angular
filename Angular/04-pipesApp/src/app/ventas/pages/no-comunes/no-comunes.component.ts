import { Component } from '@angular/core';
import { interval } from 'rxjs';

@Component({
	selector: 'app-no-comunes',
	templateUrl: './no-comunes.component.html',
	styles: [
	]
})
export class NoComunesComponent {
	// i18nSelect
	nombre: string = 'Pablo';
	genero: string = 'masculino';

	invitacionMap = {
		'masculino': 'invitarlo',
		'femenino': 'invitarla'
	}

	// i18nPlural
	clientes: string[] = ['María', 'Juan', 'Eustaquio', 'Roberto'];
	clientesMap = {
		'=0': 'no tenemos ningún clientes esperando',
		'=1': 'tenemos un cliente esperando',
		'other': 'tenemos # clientes esperando'
	}

	cambiarCliente() {
		this.nombre == 'Pablo' ? this.nombre = "María" : this.nombre = 'Pablo';
		this.genero == 'femenino' ? this.genero = "masculino" : this.genero = 'femenino';
	}

	borrarCliente() {
		if (this.clientes.length > 0) {
			this.clientes.pop();
		}
	}

	// KeyValue
	persona = {
		nombre: 'Francisco',
		edad: 35,
		direccion: 'Ciudad de México, México'
	}

	// Json
	heroes = [
		{
			nombre: 'Superman',
			vuela: true
		},
		{
			nombre: 'Robin',
			vuela: false
		},
		{
			nombre: 'Aquaman',
			vuela: false
		}
	]

	// Async
	miObservable = interval(1000); // 0,1,2,3,4,...

	valorPromesa = new Promise((resolve, reject) => {
		setTimeout(() => {
			resolve('Tenemos data de promesa');
		}, 3500);
	});
}
