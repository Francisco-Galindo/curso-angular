import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent {

	nombreLower: string = 'francisco';
	nombreupper: string = 'FRANCISCO';
	nombreCompleto: string = 'FraNcisSco gAlinDO';

	fecha: Date = new Date();
}
