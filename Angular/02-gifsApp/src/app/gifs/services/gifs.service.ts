import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { SearchGifsResponse, Gif } from '../interfaces/gifs.interfaces';

@Injectable({
	providedIn: 'root'
})
export class GifsService {

	constructor (private http: HttpClient) {

		this._historial = JSON.parse(localStorage.getItem('historial')!) || [];
		this.resultados = JSON.parse(localStorage.getItem('resultados')!) || [];
	}

	private apiKey: string = 'IJK8zitenGEDu6VPniElHaemytALRi70';
	private servicioUrl: string = 'https://api.giphy.com/v1/gifs';
	private _historial: string[] = [];

	public resultados: Gif[] = [];

	get historial() {
		return [...this._historial];
	}

	buscarGifs(query: string = '') {
		query = query.trim().toLowerCase();

		if (this._historial.includes(query)) {
			const index = this._historial.indexOf(query);
			this._historial.splice(index, 1);
		}

		const valor = this._historial.unshift(query);
		this._historial = this._historial.splice(0, 10);

		localStorage.setItem("historial", JSON.stringify(this.historial));


		const params = new HttpParams()
		.set('api_key', this.apiKey)
		.set('q', query)
		.set('limit', '10');

		this.http.get<SearchGifsResponse>(`${this.servicioUrl}/search`, {params})
		.subscribe( (resp) => {
			this.resultados = resp.data;
			localStorage.setItem('resultados', JSON.stringify(this.resultados)); });
	}
}
