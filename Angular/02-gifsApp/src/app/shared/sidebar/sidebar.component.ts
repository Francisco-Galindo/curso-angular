import { Component, OnInit } from '@angular/core';
import { GifsService } from '../../gifs/services/gifs.service';

@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
})
export class SidebarComponent implements OnInit {

	constructor(private gisfService: GifsService) { }

	get historial() {
		return [...this.gisfService.historial];
	}

	buscar(termino: string) {
		this.gisfService.buscarGifs(termino);
	}
	ngOnInit(): void {
	}

}
