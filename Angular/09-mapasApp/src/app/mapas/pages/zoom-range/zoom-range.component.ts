import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';

@Component({
	selector: 'app-zoom-range',
	templateUrl: './zoom-range.component.html',
	styles: [
		`
		.mapa-container {
			witdh: 100%;
			height: 100%;
		}

		.row {
			background-color: white;
			border-radius: 5px;
			bottom: 50px;
			left: 50px;
			padding: 10px;
			position: fixed;
			z-index: 99;
			witdh: 200px;
		}
		`
	]
})
export class ZoomRangeComponent implements AfterViewInit, OnDestroy {

	@ViewChild('mapa') divMapa!: ElementRef;
	mapa!: mapboxgl.Map;
	zoomLevel: number = 16;
	center: [number, number] = [-99.15632043877577, 19.351984256191695,];

	constructor() {
		console.log('constructor', this.divMapa);
	}

	ngOnDestroy(): void {
		this.mapa.off('zoom', () => {});
		this.mapa.off('move', () => {});
	}

	ngAfterViewInit(): void {

		this.mapa = new mapboxgl.Map({
			container: this.divMapa.nativeElement,
			style: 'mapbox://styles/mapbox/streets-v11',
			center: this.center,
			zoom: this.zoomLevel,
		});

		this.mapa.on('zoom', () => {
			this.zoomLevel = this.mapa.getZoom();
		});

		this.mapa.on('move', (event) => {
			const target = event.target;
			const {lng, lat} = target.getCenter();
			this.center = [lng, lat];
		});
	}

	zoomIn() {
		this.mapa.zoomIn();
	}

	zoomOut() {
		this.mapa.zoomOut();
	}

	zoomCambio(valor: string) {
		this.mapa.zoomTo(Number(valor));
	}
}
