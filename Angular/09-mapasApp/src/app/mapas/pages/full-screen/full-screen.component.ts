import { Component, OnInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';

@Component({
	selector: 'app-full-screen',
	templateUrl: './full-screen.component.html',
	styles: [
		`
		#mapa {
			witdh: 100%;
			height: 100%;
		}
		`
	]
})
export class FullScreenComponent implements OnInit {

	constructor() { }

	ngOnInit(): void {
		let map = new mapboxgl.Map({
			container: 'mapa',
			style: 'mapbox://styles/mapbox/streets-v11',
			center: [-99.15632043877577, 19.351984256191695,],
			zoom: 16,
		});
	}

}
