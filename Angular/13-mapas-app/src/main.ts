import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import Mapboxgl from 'mapbox-gl';

Mapboxgl.accessToken = 'pk.eyJ1IjoicGFjb2dhbGluZG8iLCJhIjoiY2t2aWM3MWZ1MnJnZTJvczExY3d6Y2xibSJ9.Pq4cEKxZhX5G3WcpKmIfJQ';

if (!navigator.geolocation) {
	alert('Navegador no soporta la Geolocalización');
	throw new Error('Navegador no soporta la Geolocalización');
}

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
