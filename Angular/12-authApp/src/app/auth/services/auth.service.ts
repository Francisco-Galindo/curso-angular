import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {AuthResponse, Usuario} from '../interfaces/interfaces';
import {catchError, map, tap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	private baseUrl: string = environment.baseUrl;
	private _usuario!: Usuario;

	get usuario() {
		return {...this._usuario};
	}

	constructor(private http: HttpClient) { }

	register(email: string, password: string, name: string) {
		const url = `${this.baseUrl}/auth/new`;
		const body = {name, password, email}


		return this.http.post<AuthResponse>(url, body)
			.pipe(
				tap(resp => {
					if (resp.ok) {
						localStorage.setItem('token', resp.token!);
					}
				}),
				map(resp => resp.ok),
				catchError(err => of(err.error.msg))
			);
	}

	login(email: string, password: string) {
		const url = `${this.baseUrl}/auth`;
		const body = {password, email}

		return this.http.post<AuthResponse>(url, body)
			.pipe(
				tap(resp => {
					if (resp.ok) {
						localStorage.setItem('token', resp.token!);
					}
				}),
				map(resp => resp.ok),
				catchError(err => of(err.error.msg))
			);
	}

	logout() {
		localStorage.clear();
	}

	validarToken(): Observable<boolean> {
		const url = `${this.baseUrl}/auth/renew`;
		const headers = new HttpHeaders()
			.set('x-token', localStorage.getItem('token') || '');

		return this.http.get<AuthResponse>(url, {headers})
			.pipe(
				map(resp => {
					this._usuario = {
						name: resp.name!,
						uid: resp.uid!,
						email: resp.email!
					}
					console.log(resp.token!);
					return resp.ok;
				}),
				catchError(err => of(false))
			);
	}

}
