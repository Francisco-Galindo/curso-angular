import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

import Swal from 'sweetalert2';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: [
  ]
})
export class RegisterComponent {
	miFormlario: FormGroup = this.fb.group({
		email: ['test1@test.com', [Validators.required, Validators.email]],
		password: ['123123', [Validators.required, Validators.minLength(6)]],
		name: ['Test Jameson', [Validators.required]],
	});

	constructor(private fb: FormBuilder,
		   private router: Router,
		   private authService: AuthService) { }

	login() {
		this.register();
	}

	register() {
		const {email, password, name} = this.miFormlario.value;

		this.authService.register(email, password, name)
			.subscribe(resp => {
				console.log(resp);
				if (resp === true) {
					this.router.navigateByUrl('/dashboard');
				} else {
					Swal.fire('Error', resp, 'error');
				}
			});
	}
}
