import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Personaje } from '../interfaces/dbz.interface';
import { DbzService } from '../services/dbz.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
})
export class AgregarComponent {

	constructor(private dbzService: DbzService) {}

	personajeVacio: Personaje = {
		nombre: '',
		poder: 0
	}

	@Input() nuevo: Personaje = this.personajeVacio;

	//@Output() onNewCharacter: EventEmitter<Personaje> = new EventEmitter();

	agregar() {
		if (this.nuevo.nombre.trim().length === 0) {
			return;
		}

		this.dbzService.agregarPersonaje(this.nuevo);

		//this.onNewCharacter.emit(this.nuevo);

		this.nuevo = this.personajeVacio;
	}
}
