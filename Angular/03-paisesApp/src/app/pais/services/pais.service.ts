import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Country } from '../interfaces/pais.interface';

@Injectable({
	providedIn: 'root'
})
export class PaisService {

	constructor(private http: HttpClient ) { }

	private apiUrl: string = 'https://restcountries.eu/rest/v2';
	filters: string = '?fields=name;capital;alpha2Code;flag;population';

	buscarPais(termino: string): Observable<Country[]> {

		const url = `${this.apiUrl}/name/${termino}${this.filters}`;
		return this.http.get<Country[]>(url);
	}

	buscarCapital(termino: string): Observable<Country[]> {

		const url = `${this.apiUrl}/capital/${termino}${this.filters}`;
		return this.http.get<Country[]>(url);
	}

	verPais(termino: string): Observable<Country> {

		const url = `${this.apiUrl}/alpha/${termino}`;
		return this.http.get<Country>(url);
	}

	buscarRegion(termino: string): Observable<Country[]> {

		const url = `${this.apiUrl}/region/${termino}${this.filters}`;
		return this.http.get<Country[]>(url);
	}
}
