import { Component, OnInit } from '@angular/core';

import { PaisService } from '../../services/pais.service';
import { Country } from '../../interfaces/pais.interface';


@Component({
  selector: 'app-por-capital',
  templateUrl: './por-capital.component.html',
  styles: [
  ]
})
export class PorCapitalComponent implements OnInit {

	constructor(private paisService: PaisService) { }

	termino: string = '';
	hayError: boolean = false;
	paises: Country[] = [];

	ngOnInit(): void {
	}

	buscar(termino: string) {
		this.hayError = false;
		this.termino = termino;

		this.paisService.buscarCapital(this.termino)
		.subscribe((paises) => {
			this.paises = paises;
			console.log(paises);
		}, err => {
			this.hayError = true;
			this.paises = [];
		});
	}

}
