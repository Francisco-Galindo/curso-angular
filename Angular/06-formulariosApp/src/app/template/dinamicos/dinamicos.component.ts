import { Component } from '@angular/core';

interface Favorito {
	id: number;
	nombre: string;
}

interface Persona {
	nombre: string;
	favoritos: Favorito[];
}

@Component({
	selector: 'app-dinamicos',
	templateUrl: './dinamicos.component.html',
	styles: [
	]
})

export class DinamicosComponent {

	persona: Persona = {
		nombre: 'Fernando',
		favoritos: [
			{id: 1, nombre: 'Metal Gear'},
			{id: 2, nombre: 'TLOZ'},
		]
	}
	nuevoJuego: string = '';

	constructor() { }

	ngOnInit(): void {
	}

	guardar() {
		// this.persona.favoritos.push();
	}

	agregarJuego() {
		const nuevoFavorito: Favorito = {
			id: this.persona.favoritos.length + 1,
			nombre: this.nuevoJuego
		};

		this.persona.favoritos.push(nuevoFavorito);
		this.nuevoJuego = '';
	}

	eliminar(index: number) {
		this.persona.favoritos.splice(index, 1);
	}
}
