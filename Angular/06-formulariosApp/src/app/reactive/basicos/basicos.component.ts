import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
	selector: 'app-basicos',
	templateUrl: './basicos.component.html',
	styles: [
	]
})
export class BasicosComponent implements OnInit {

	// miFormulario: FormGroup = new FormGroup({
	// 	'nombre': new FormControl('Juguete feo'),
	// 	'precio': new FormControl(1200),
	// 	'existencias': new FormControl(30),
	// });

	miFormulario: FormGroup = this.fb.group({
		nombre: [null, [Validators.required, Validators.minLength(3)]],
		precio: [null, Validators.min(0)],
		existencias: [null, Validators.min(0)],

	})

	constructor(private fb: FormBuilder) { }

	ngOnInit(): void {
		this.miFormulario.reset({
			nombre: 'Juguete feo',
			precio: 1200
		});
	}

	campoEsInvalido(campo: string) {
		return this.miFormulario.controls[campo].errors &&
			this.miFormulario.controls[campo].touched;
	}

	guardar(){
		if (this.miFormulario.invalid) {
			this.miFormulario.markAllAsTouched();
			return;
		}
		console.log(this.miFormulario.value);
		this.miFormulario.reset();
	}

}
