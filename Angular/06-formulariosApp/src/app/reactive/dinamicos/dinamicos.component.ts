import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
	selector: 'app-dinamicos',
	templateUrl: './dinamicos.component.html',
	styles: [
	]
})
export class DinamicosComponent implements OnInit {


	// miFormulario: FormGroup = new FormGroup({
	// 	'nombre': new FormControl('Juguete feo'),
	// 	'precio': new FormControl(1200),
	// 	'existencias': new FormControl(30),
	// });

	miFormulario: FormGroup = this.fb.group({
		nombre: [null, [Validators.required, Validators.minLength(3)]],
		favoritos: this.fb.array([
			['Hola', Validators.required],
			['Bye', Validators.required],
		], Validators.required),

	})

	nuevoFavorito: FormControl = this.fb.control(null, [Validators.required]);

	get favoritosArr() {
		return this.miFormulario.get('favoritos') as FormArray;
	}

	constructor(private fb: FormBuilder) { }

	ngOnInit(): void {
	}

	campoEsInvalido(campo: string) {
		return this.miFormulario.controls[campo].errors &&
			this.miFormulario.controls[campo].touched;
	}

	agregarFavorito() {
		if (this.nuevoFavorito.invalid) {
			return;
		}

		this.favoritosArr.push(new FormControl(this.nuevoFavorito.value));
		this.nuevoFavorito.reset();
	}

	borrar(i: number) {
		this.favoritosArr.removeAt(i);
	}

	guardar() {
		this.miFormulario.markAllAsTouched();
		if (this.miFormulario.invalid) {
			return;
		}
		console.log(this.miFormulario.value);
	}
}
