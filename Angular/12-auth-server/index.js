const express = require('express');
const cors = require('cors');
const {dbConnection} = require('./db/config');
require('dotenv').config();

const app = express();

dbConnection();

// Directorio público
app.use(express.static('public'));

app.use(cors());

app.use(express.json());

// Rutas
app.use('/api/auth', require('./routes/auth'));

app.listen(process.env.PORT, () => {
    console.log(`Servidor corriendo en el puerto ${process.env.PORT}`);
});
