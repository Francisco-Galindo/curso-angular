const Usuario = require('../models/Usuario');
const bcrypt =require('bcryptjs');
const {generarJWT} = require('../helpers/jwt')

const creaUsuario = async (req, res) => {
    const {email, name, password} = req.body;

    try {
        const usuario = await Usuario.findOne({email: email})
        if (usuario) {
            return res.status(400).json({
                ok: false,
                msg: 'El usuario ya existe'
            });
        }

        const dbUser = new Usuario(req.body);

        const salt = bcrypt.genSaltSync();
        dbUser.password = bcrypt.hashSync(password, salt);

        const token = await generarJWT(dbUser.id, dbUser.name);

        await dbUser.save();

        return res.status(201).json({
            ok: true,
            uid: dbUser.id,
            name: name,
            email: dbUser.email,
            token: token
        });

    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Porfavor, hable con el administrador'
        })
    }
}

const loginUsuario = async (req, res) => {
    const {email, password} = req.body;

    try {
        const dbUser = await Usuario.findOne({email: email});
        if (!dbUser) {
            return res.status(400).json({
                ok: false,
                msg: 'Datos incorrectos'
            });
        }

        const contra = bcrypt.compareSync(password, dbUser.password);
        if (!contra) {
            return res.status(400).json({
                ok: false,
                msg: 'Datos incorrectos'
            })
        }

        const token = await generarJWT(dbUser.id, dbUser.name);

        return res.json({
            ok: true,
            uid: dbUser.id,
            name: dbUser.name,
            email: dbUser.email,
            token: token
        })

    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Porfavor, hable con el administrador'
        })
    }
}

// Validar y revalidar token
const revalidarToken = async (req, res) => {
    const {uid} = req;

    const dbUser = await Usuario.findById(uid);

    const token = await generarJWT(uid, dbUser.name);

    return res.json({
        ok: true,
        uid: uid,
        name: dbUser.name,
        email: dbUser.email,
        token: token
    })
}

module.exports = {
    creaUsuario,
    loginUsuario,
    revalidarToken
}
