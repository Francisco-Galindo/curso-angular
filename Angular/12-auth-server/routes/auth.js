const {Router} = require('express');
const {check} = require('express-validator');
const {creaUsuario, loginUsuario, revalidarToken} = require('../controllers/auth');
const {validarCampos} = require('../middlewares/validar-campos');
const {validarJWT} = require('../middlewares/validar-jwt');

const router = Router();

// Crear nuevo usuario
router.post(
    '/new',
    [
        check('name', 'El nombre es obligatorio').not().isEmpty(),
        check('email', 'El email es obligatorio').isEmail(),
        check('password', 'La contraseña es obligatoria y debe ser buena').not().isEmpty(),
        validarCampos
    ],
    creaUsuario
);

// Login
router.post(
    '/',
    [
        check('email', 'El email es obligatorio').isEmail(),
        check('password', 'La contraseña es obligatoria').not().isEmpty(),
        validarCampos
    ],
    loginUsuario
);

// Validar y revalidar token
router.get('/renew', validarJWT, revalidarToken);

module.exports = router;
