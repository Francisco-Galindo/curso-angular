interface Reproductor {
	volumen: number;
	segundo: number;
	cancion: string;
	detalles: Detalles;
}

interface Detalles {
	autor: string;
	anno: number;
}

const reproductor: Reproductor = {
	volumen: 90,
	segundo: 36,
	cancion: 'Mess',
	detalles: {
		autor: 'Ed Sheeran',
		anno: 2015
	}
}

/* Desestructurando un objeto */
const {volumen, segundo, cancion, detalles} = reproductor;
const {autor} = detalles;

/*
console.log('El volumen actual es de: ', volumen);
console.log('El segundo actual es de: ', segundo);
console.log('La canción actual es de: ', cancion);
console.log('El autor es: ', autor);
*/


const dbz: string[] = ['Goku', 'Vegeta', 'Trunks'];
/* Desestructurando un arreglo */
const [goku, vegeta, trunks] = dbz;
const [, , trunks_otra_vez] = dbz;

console.log('Personaje 1: ', goku);
console.log('Personaje 2: ', vegeta);
console.log('Personaje 3: ', trunks);
console.log('Personaje 3: ', trunks_otra_vez);
