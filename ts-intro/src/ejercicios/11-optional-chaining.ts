interface Pasajero {
	nombre: string;
	hijos?: string[]
}

const pasajero: Pasajero = {
	nombre: 'Fernando'
}

const pasajero2: Pasajero = {
	nombre: 'Melissa',
	hijos: ['Natalia', 'Gabriel']
}

function imprimeHijos(pasajero: Pasajero): void {
	const numHijos = pasajero.hijos?.length || 0;

	console.log(numHijos);
}

imprimeHijos(pasajero);
