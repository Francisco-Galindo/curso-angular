function queTipoSoy<T>(argumento: T) {
	return argumento;
}


let soyString = queTipoSoy('Hola Mundo');
let soyNumero = queTipoSoy(100);
let soyArreglo = queTipoSoy([1, 3, 7]);

let soyExpicito = queTipoSoy<string>('Hola mundo');


