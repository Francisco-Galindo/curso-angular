export interface Producto {
	desc: string;
	precio: number;
}

const telefono: Producto = {
	desc: 'Nokia chido',
	precio: 150
}

const tableta: Producto = {
	desc: 'iPad chafa',
	precio: 234
}


export function calculaImpuestoVenta(productos: Producto[]): [number, number] {

	let total = 0;

	productos.forEach( ({precio}) => {
		total += precio;
	})

	return [total, total * 0.15];
}

const articulos = [telefono, tableta];

const [total, isv] = calculaImpuestoVenta(articulos);

console.log('ISV: ', isv);
